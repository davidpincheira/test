const express = require('express')
const path = require('path')
const mutant = require('./mutants.js')
const Mutation = require("./model/mutation.js")
const app = express()

app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.use(express.static(path.join(__dirname, 'public')))

let respuesta = {
    error: false,
    codigo: 200,
    mensaje: 'OK'
};

app.get('/stats', function (req, res) {
    Mutation.findAll().then(mutation => 
        res.json(mutation)
    )
});

app.post('/mutation', function (req, res) {

    let adn = req.body.dna
    let esMutant = mutant.isMutant(adn)

    if(esMutant == true){
        var arr = adn + ''
        
        Mutation.create({
            adn: arr
        }).then(mutation=>{
            //res.json(mutation)
            respuesta = {
                error: false,
                codigo: 200,
                mensaje: 'OK',
                mutation: mutation
            };
        }).catch(err=>{
            //res.json(err)
            respuesta = {
                error: true,
                codigo: 403,
                mensaje: 'Forbidden'
            };
        })
    } else {
        respuesta = {
            error: true,
            codigo: 403,
            mensaje: 'Forbidden'
        };
    }
    res.json(respuesta);
});

app.listen(process.env.PORT || 3000, ()=>{
    console.log('server corriendo');
})

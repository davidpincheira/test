const Sequelize = require('sequelize')

  const sequelize = new Sequelize('test', 'root', 'root', {
    host: '127.0.0.1',
    dialect: 'sqlite',
  })
  
  sequelize.sync({ force: true })

  module.exports = sequelize
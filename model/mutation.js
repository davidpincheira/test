const sequelize  = require("./../database/sequelize.js")
const Sequelize = require('sequelize')

module.exports = sequelize.define('mutation', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
    },
    adn: {
        type: Sequelize.STRING,
        unique: { //validacion para que no se repita
            args: true,
            msg: 'La mutacion ya existe'
        }
    }
},{ 
    timeStamp: false 
}, {
    freezeTableName: true 
  })